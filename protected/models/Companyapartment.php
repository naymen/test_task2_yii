<?php

/**
 * This is the model class for table "ur_companyapartment".
 *
 * The followings are the available columns in table 'ur_companyapartment':
 * @property string $companyapartment_id
 * @property integer $discount
 * @property string $discount_description
 * @property string $weekday_from
 * @property string $weekday_to
 * @property string $holiday_from
 * @property string $holiday_to
 * @property string $event_time
 * @property string $title
 * @property string $info
 * @property string $announcement
 * @property integer $has_logo
 * @property integer $public_state
 * @property integer $status
 * @property integer $is_popular
 * @property string $decline_cause
 * @property integer $advert_type
 * @property string $paid_before
 * @property integer $comment_count
 * @property string $creation_time
 * @property string $update_time
 * @property integer $f_category_id
 * @property string $f_user_id
 * @property integer $paid_link
 * @property string $utime
 * @property integer $auto_update
 * @property string $date_auto_update
 * @property string $last_auto_update
 *
 * The followings are the available model relations:
 * @property UrCategory $fCategory
 * @property UrUser $fUser
 * @property UrCounter $companyapartment
 */
class Companyapartment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ur_companyapartment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('companyapartment_id, weekday_from, weekday_to, holiday_from, holiday_to, f_category_id, f_user_id, utime, auto_update, date_auto_update, last_auto_update', 'required'),
			array('discount, has_logo, public_state, status, is_popular, advert_type, comment_count, f_category_id, paid_link, auto_update', 'numerical', 'integerOnly'=>true),
			array('companyapartment_id, f_user_id', 'length', 'max'=>20),
			array('title', 'length', 'max'=>250),
			array('discount_description, event_time, info, announcement, decline_cause, paid_before, creation_time, update_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('companyapartment_id, discount, discount_description, weekday_from, weekday_to, holiday_from, holiday_to, event_time, title, info, announcement, has_logo, public_state, status, is_popular, decline_cause, advert_type, paid_before, comment_count, creation_time, update_time, f_category_id, f_user_id, paid_link, utime, auto_update, date_auto_update, last_auto_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fCategory' => array(self::BELONGS_TO, 'UrCategory', 'f_category_id'),
			'fUser' => array(self::BELONGS_TO, 'UrUser', 'f_user_id'),
			'companyapartment' => array(self::BELONGS_TO, 'UrCounter', 'companyapartment_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'companyapartment_id' => 'Companyapartment',
			'discount' => 'Discount',
			'discount_description' => 'Discount Description',
			'weekday_from' => 'Weekday From',
			'weekday_to' => 'Weekday To',
			'holiday_from' => 'Holiday From',
			'holiday_to' => 'Holiday To',
			'event_time' => 'Event Time',
			'title' => 'Title',
			'info' => 'Info',
			'announcement' => 'Announcement',
			'has_logo' => 'Has Logo',
			'public_state' => 'Public State',
			'status' => 'Status',
			'is_popular' => 'Is Popular',
			'decline_cause' => 'Decline Cause',
			'advert_type' => 'Advert Type',
			'paid_before' => 'Paid Before',
			'comment_count' => 'Comment Count',
			'creation_time' => 'Creation Time',
			'update_time' => 'Update Time',
			'f_category_id' => 'F Category',
			'f_user_id' => 'F User',
			'paid_link' => 'Paid Link',
			'utime' => 'Utime',
			'auto_update' => 'Auto Update',
			'date_auto_update' => 'Date Auto Update',
			'last_auto_update' => 'Last Auto Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('companyapartment_id',$this->companyapartment_id,true);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('discount_description',$this->discount_description,true);
		$criteria->compare('weekday_from',$this->weekday_from,true);
		$criteria->compare('weekday_to',$this->weekday_to,true);
		$criteria->compare('holiday_from',$this->holiday_from,true);
		$criteria->compare('holiday_to',$this->holiday_to,true);
		$criteria->compare('event_time',$this->event_time,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('announcement',$this->announcement,true);
		$criteria->compare('has_logo',$this->has_logo);
		$criteria->compare('public_state',$this->public_state);
		$criteria->compare('status',$this->status);
		$criteria->compare('is_popular',$this->is_popular);
		$criteria->compare('decline_cause',$this->decline_cause,true);
		$criteria->compare('advert_type',$this->advert_type);
		$criteria->compare('paid_before',$this->paid_before,true);
		$criteria->compare('comment_count',$this->comment_count);
		$criteria->compare('creation_time',$this->creation_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('f_category_id',$this->f_category_id);
		$criteria->compare('f_user_id',$this->f_user_id,true);
		$criteria->compare('paid_link',$this->paid_link);
		$criteria->compare('utime',$this->utime,true);
		$criteria->compare('auto_update',$this->auto_update);
		$criteria->compare('date_auto_update',$this->date_auto_update,true);
		$criteria->compare('last_auto_update',$this->last_auto_update,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Companyapartment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
