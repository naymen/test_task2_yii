<?php

/**
 * This is the model class for table "ur_counter".
 *
 * The followings are the available columns in table 'ur_counter':
 * @property string $counter_id
 * @property integer $cnt
 * @property integer $f_type_id
 *
 * The followings are the available model relations:
 * @property UrApartment $urApartment
 * @property UrApartment[] $urApartments
 * @property UrCompanyapartment $urCompanyapartment
 * @property UrType $fType
 * @property UrUser[] $urUsers
 */
class Counter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ur_counter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('f_type_id', 'required'),
			array('cnt, f_type_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('counter_id, cnt, f_type_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'urApartment' => array(self::HAS_ONE, 'UrApartment', 'apartment_id'),
			'urApartments' => array(self::HAS_MANY, 'UrApartment', 'f_company_id'),
			'urCompanyapartment' => array(self::HAS_ONE, 'UrCompanyapartment', 'companyapartment_id'),
			'fType' => array(self::BELONGS_TO, 'UrType', 'f_type_id'),
			'urUsers' => array(self::HAS_MANY, 'UrUser', 'f_company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'counter_id' => 'Counter',
			'cnt' => 'Cnt',
			'f_type_id' => 'F Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('counter_id',$this->counter_id,true);
		$criteria->compare('cnt',$this->cnt);
		$criteria->compare('f_type_id',$this->f_type_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Counter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
