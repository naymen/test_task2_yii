<?php

/**
 * This is the model class for table "ur_user".
 *
 * The followings are the available columns in table 'ur_user':
 * @property string $user_id
 * @property string $email
 * @property string $password
 * @property integer $f_district_id
 * @property integer $status
 * @property string $user_key
 * @property string $register_date
 * @property string $confirm_date
 * @property integer $role
 * @property string $auth_id
 * @property string $service
 * @property string $fio
 * @property string $employment
 * @property string $phone
 * @property string $f_company_id
 * @property string $last_visit_date
 * @property string $login
 * @property integer $sex
 * @property string $first_name
 * @property string $last_name
 * @property string $father_name
 * @property string $contact_email
 * @property string $skype
 * @property string $info
 * @property string $education
 * @property string $work
 * @property string $birthday
 * @property integer $has_logo
 * @property string $member_date
 * @property string $f_repetitor_subject_id
 * @property string $update_time
 * @property string $utime
 * @property integer $f_city_id
 *
 * The followings are the available model relations:
 * @property UrApartment[] $urApartments
 * @property UrCompanyapartment[] $urCompanyapartments
 * @property UrDistrict $fDistrict
 * @property UrCity $fCity
 * @property UrCounter $fCompany
 * @property UrRepetitorSubject $fRepetitorSubject
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ur_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, password, f_district_id, user_key, register_date, update_time, utime', 'required'),
			array('f_district_id, status, role, sex, has_logo, f_city_id', 'numerical', 'integerOnly'=>true),
			array('email, auth_id', 'length', 'max'=>250),
			array('password', 'length', 'max'=>65),
			array('user_key', 'length', 'max'=>32),
			array('service, login', 'length', 'max'=>45),
			array('fio, employment', 'length', 'max'=>150),
			array('phone', 'length', 'max'=>100),
			array('f_company_id, f_repetitor_subject_id', 'length', 'max'=>20),
			array('first_name, last_name, father_name', 'length', 'max'=>49),
			array('contact_email, skype', 'length', 'max'=>50),
			array('confirm_date, last_visit_date, info, education, work, birthday, member_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, email, password, f_district_id, status, user_key, register_date, confirm_date, role, auth_id, service, fio, employment, phone, f_company_id, last_visit_date, login, sex, first_name, last_name, father_name, contact_email, skype, info, education, work, birthday, has_logo, member_date, f_repetitor_subject_id, update_time, utime, f_city_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'urApartments' => array(self::HAS_MANY, 'UrApartment', 'f_user_id'),
			'urCompanyapartments' => array(self::HAS_MANY, 'UrCompanyapartment', 'f_user_id'),
			'fDistrict' => array(self::BELONGS_TO, 'UrDistrict', 'f_district_id'),
			'fCity' => array(self::BELONGS_TO, 'UrCity', 'f_city_id'),
			'fCompany' => array(self::BELONGS_TO, 'UrCounter', 'f_company_id'),
			'fRepetitorSubject' => array(self::BELONGS_TO, 'UrRepetitorSubject', 'f_repetitor_subject_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'email' => 'Email',
			'password' => 'Password',
			'f_district_id' => 'F District',
			'status' => 'Status',
			'user_key' => 'User Key',
			'register_date' => 'Register Date',
			'confirm_date' => 'Confirm Date',
			'role' => 'Role',
			'auth_id' => 'Auth',
			'service' => 'Service',
			'fio' => 'Fio',
			'employment' => 'Employment',
			'phone' => 'Phone',
			'f_company_id' => 'F Company',
			'last_visit_date' => 'Last Visit Date',
			'login' => 'Login',
			'sex' => 'Sex',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'father_name' => 'Father Name',
			'contact_email' => 'Contact Email',
			'skype' => 'Skype',
			'info' => 'Info',
			'education' => 'Education',
			'work' => 'Work',
			'birthday' => 'Birthday',
			'has_logo' => 'Has Logo',
			'member_date' => 'Member Date',
			'f_repetitor_subject_id' => 'F Repetitor Subject',
			'update_time' => 'Update Time',
			'utime' => 'Utime',
			'f_city_id' => 'F City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('f_district_id',$this->f_district_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('user_key',$this->user_key,true);
		$criteria->compare('register_date',$this->register_date,true);
		$criteria->compare('confirm_date',$this->confirm_date,true);
		$criteria->compare('role',$this->role);
		$criteria->compare('auth_id',$this->auth_id,true);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('employment',$this->employment,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('f_company_id',$this->f_company_id,true);
		$criteria->compare('last_visit_date',$this->last_visit_date,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('father_name',$this->father_name,true);
		$criteria->compare('contact_email',$this->contact_email,true);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('education',$this->education,true);
		$criteria->compare('work',$this->work,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('has_logo',$this->has_logo);
		$criteria->compare('member_date',$this->member_date,true);
		$criteria->compare('f_repetitor_subject_id',$this->f_repetitor_subject_id,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('utime',$this->utime,true);
		$criteria->compare('f_city_id',$this->f_city_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
