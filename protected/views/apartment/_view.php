<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('apartment_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->apartment_id),array('view','id'=>$data->apartment_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('_old_id')); ?>:</b>
	<?php echo CHtml::encode($data->_old_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cost')); ?>:</b>
	<?php echo CHtml::encode($data->cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('appliances')); ?>:</b>
	<?php echo CHtml::encode($data->appliances); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('furniture')); ?>:</b>
	<?php echo CHtml::encode($data->furniture); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('storey')); ?>:</b>
	<?php echo CHtml::encode($data->storey); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('storey_count')); ?>:</b>
	<?php echo CHtml::encode($data->storey_count); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('contact')); ?>:</b>
	<?php echo CHtml::encode($data->contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('square')); ?>:</b>
	<?php echo CHtml::encode($data->square); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('square_life')); ?>:</b>
	<?php echo CHtml::encode($data->square_life); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_company_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_company_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('info')); ?>:</b>
	<?php echo CHtml::encode($data->info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skype')); ?>:</b>
	<?php echo CHtml::encode($data->skype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('has_logo')); ?>:</b>
	<?php echo CHtml::encode($data->has_logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('public_state')); ?>:</b>
	<?php echo CHtml::encode($data->public_state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creation_time')); ?>:</b>
	<?php echo CHtml::encode($data->creation_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decline_cause')); ?>:</b>
	<?php echo CHtml::encode($data->decline_cause); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_category_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_sub_category_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_sub_category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment_count')); ?>:</b>
	<?php echo CHtml::encode($data->comment_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_city_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_city_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_district_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_district_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paid_before')); ?>:</b>
	<?php echo CHtml::encode($data->paid_before); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('advert_type')); ?>:</b>
	<?php echo CHtml::encode($data->advert_type); ?>
	<br />

	*/ ?>

</div>