<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      apartment_id		</th>
 		<th width="80px">
		      _old_id		</th>
 		<th width="80px">
		      cost		</th>
 		<th width="80px">
		      appliances		</th>
 		<th width="80px">
		      furniture		</th>
 		<th width="80px">
		      storey		</th>
 		<th width="80px">
		      storey_count		</th>
 		<th width="80px">
		      contact		</th>
 		<th width="80px">
		      square		</th>
 		<th width="80px">
		      square_life		</th>
 		<th width="80px">
		      f_company_id		</th>
 		<th width="80px">
		      title		</th>
 		<th width="80px">
		      address		</th>
 		<th width="80px">
		      info		</th>
 		<th width="80px">
		      phone		</th>
 		<th width="80px">
		      email		</th>
 		<th width="80px">
		      skype		</th>
 		<th width="80px">
		      has_logo		</th>
 		<th width="80px">
		      public_state		</th>
 		<th width="80px">
		      status		</th>
 		<th width="80px">
		      creation_time		</th>
 		<th width="80px">
		      update_time		</th>
 		<th width="80px">
		      decline_cause		</th>
 		<th width="80px">
		      f_user_id		</th>
 		<th width="80px">
		      f_category_id		</th>
 		<th width="80px">
		      f_sub_category_id		</th>
 		<th width="80px">
		      comment_count		</th>
 		<th width="80px">
		      f_city_id		</th>
 		<th width="80px">
		      f_district_id		</th>
 		<th width="80px">
		      paid_before		</th>
 		<th width="80px">
		      advert_type		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->apartment_id; ?>
		</td>
       		<td>
			<?php echo $row->_old_id; ?>
		</td>
       		<td>
			<?php echo $row->cost; ?>
		</td>
       		<td>
			<?php echo $row->appliances; ?>
		</td>
       		<td>
			<?php echo $row->furniture; ?>
		</td>
       		<td>
			<?php echo $row->storey; ?>
		</td>
       		<td>
			<?php echo $row->storey_count; ?>
		</td>
       		<td>
			<?php echo $row->contact; ?>
		</td>
       		<td>
			<?php echo $row->square; ?>
		</td>
       		<td>
			<?php echo $row->square_life; ?>
		</td>
       		<td>
			<?php echo $row->f_company_id; ?>
		</td>
       		<td>
			<?php echo $row->title; ?>
		</td>
       		<td>
			<?php echo $row->address; ?>
		</td>
       		<td>
			<?php echo $row->info; ?>
		</td>
       		<td>
			<?php echo $row->phone; ?>
		</td>
       		<td>
			<?php echo $row->email; ?>
		</td>
       		<td>
			<?php echo $row->skype; ?>
		</td>
       		<td>
			<?php echo $row->has_logo; ?>
		</td>
       		<td>
			<?php echo $row->public_state; ?>
		</td>
       		<td>
			<?php echo $row->status; ?>
		</td>
       		<td>
			<?php echo $row->creation_time; ?>
		</td>
       		<td>
			<?php echo $row->update_time; ?>
		</td>
       		<td>
			<?php echo $row->decline_cause; ?>
		</td>
       		<td>
			<?php echo $row->f_user_id; ?>
		</td>
       		<td>
			<?php echo $row->f_category_id; ?>
		</td>
       		<td>
			<?php echo $row->f_sub_category_id; ?>
		</td>
       		<td>
			<?php echo $row->comment_count; ?>
		</td>
       		<td>
			<?php echo $row->f_city_id; ?>
		</td>
       		<td>
			<?php echo $row->f_district_id; ?>
		</td>
       		<td>
			<?php echo $row->paid_before; ?>
		</td>
       		<td>
			<?php echo $row->advert_type; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
