<div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'apartment-form',
	'enableAjaxValidation'=>false,
        'method'=>'post',
	'type'=>'horizontal',
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data'
	)
)); ?>
     	<fieldset>
		<legend>
			<p class="note">Fields with <span class="required">*</span> are required.</p>
		</legend>

	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
   <div class="control-group">		
			<div class="span4">

	<?php echo $form->textFieldRow($model,'apartment_id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'_old_id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'cost',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'appliances',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'furniture',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'storey',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'storey_count',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'contact',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'square',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'square_life',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'f_company_id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textFieldRow($model,'address',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textAreaRow($model,'info',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'skype',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'has_logo',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'public_state',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'creation_time',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'update_time',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'decline_cause',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'f_user_id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'f_category_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'f_sub_category_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'comment_count',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'f_city_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'f_district_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'paid_before',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'advert_type',array('class'=>'span5')); ?>

                        </div>   
  </div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
                        'icon'=>'ok white',  
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
              <?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'reset',
                        'icon'=>'remove',  
			'label'=>'Reset',
		)); ?>
	</div>
</fieldset>

<?php $this->endWidget(); ?>

</div>
