<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      companyapartment_id		</th>
 		<th width="80px">
		      discount		</th>
 		<th width="80px">
		      discount_description		</th>
 		<th width="80px">
		      weekday_from		</th>
 		<th width="80px">
		      weekday_to		</th>
 		<th width="80px">
		      holiday_from		</th>
 		<th width="80px">
		      holiday_to		</th>
 		<th width="80px">
		      event_time		</th>
 		<th width="80px">
		      title		</th>
 		<th width="80px">
		      info		</th>
 		<th width="80px">
		      announcement		</th>
 		<th width="80px">
		      has_logo		</th>
 		<th width="80px">
		      public_state		</th>
 		<th width="80px">
		      status		</th>
 		<th width="80px">
		      is_popular		</th>
 		<th width="80px">
		      decline_cause		</th>
 		<th width="80px">
		      advert_type		</th>
 		<th width="80px">
		      paid_before		</th>
 		<th width="80px">
		      comment_count		</th>
 		<th width="80px">
		      creation_time		</th>
 		<th width="80px">
		      update_time		</th>
 		<th width="80px">
		      f_category_id		</th>
 		<th width="80px">
		      f_user_id		</th>
 		<th width="80px">
		      paid_link		</th>
 		<th width="80px">
		      utime		</th>
 		<th width="80px">
		      auto_update		</th>
 		<th width="80px">
		      date_auto_update		</th>
 		<th width="80px">
		      last_auto_update		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->companyapartment_id; ?>
		</td>
       		<td>
			<?php echo $row->discount; ?>
		</td>
       		<td>
			<?php echo $row->discount_description; ?>
		</td>
       		<td>
			<?php echo $row->weekday_from; ?>
		</td>
       		<td>
			<?php echo $row->weekday_to; ?>
		</td>
       		<td>
			<?php echo $row->holiday_from; ?>
		</td>
       		<td>
			<?php echo $row->holiday_to; ?>
		</td>
       		<td>
			<?php echo $row->event_time; ?>
		</td>
       		<td>
			<?php echo $row->title; ?>
		</td>
       		<td>
			<?php echo $row->info; ?>
		</td>
       		<td>
			<?php echo $row->announcement; ?>
		</td>
       		<td>
			<?php echo $row->has_logo; ?>
		</td>
       		<td>
			<?php echo $row->public_state; ?>
		</td>
       		<td>
			<?php echo $row->status; ?>
		</td>
       		<td>
			<?php echo $row->is_popular; ?>
		</td>
       		<td>
			<?php echo $row->decline_cause; ?>
		</td>
       		<td>
			<?php echo $row->advert_type; ?>
		</td>
       		<td>
			<?php echo $row->paid_before; ?>
		</td>
       		<td>
			<?php echo $row->comment_count; ?>
		</td>
       		<td>
			<?php echo $row->creation_time; ?>
		</td>
       		<td>
			<?php echo $row->update_time; ?>
		</td>
       		<td>
			<?php echo $row->f_category_id; ?>
		</td>
       		<td>
			<?php echo $row->f_user_id; ?>
		</td>
       		<td>
			<?php echo $row->paid_link; ?>
		</td>
       		<td>
			<?php echo $row->utime; ?>
		</td>
       		<td>
			<?php echo $row->auto_update; ?>
		</td>
       		<td>
			<?php echo $row->date_auto_update; ?>
		</td>
       		<td>
			<?php echo $row->last_auto_update; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
