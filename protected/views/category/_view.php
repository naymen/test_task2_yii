<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->category_id),array('view','id'=>$data->category_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('list_pos')); ?>:</b>
	<?php echo CHtml::encode($data->list_pos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('translit')); ?>:</b>
	<?php echo CHtml::encode($data->translit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('_old_id')); ?>:</b>
	<?php echo CHtml::encode($data->_old_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_title')); ?>:</b>
	<?php echo CHtml::encode($data->seo_title); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_description')); ?>:</b>
	<?php echo CHtml::encode($data->seo_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_keywords')); ?>:</b>
	<?php echo CHtml::encode($data->seo_keywords); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_texttop')); ?>:</b>
	<?php echo CHtml::encode($data->seo_texttop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_textbottom')); ?>:</b>
	<?php echo CHtml::encode($data->seo_textbottom); ?>
	<br />

	*/ ?>

</div>