<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <!--<link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />-->

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                 
                            

                            array('label'=>'Объявления', 'url'=>'#', 'items'=>array(

                                array('label'=>'Жилье', 'url'=>array('apartment/index')),


                            )),

                            

                            array('label'=>'Компании', 'url'=>'#', 'items'=>array(

                               

                                array('label'=>'Жильё', 'url'=>array('companyapartment/index')),

                               

                            )),   

                            

                            array('label'=>'Категории', 'url'=>'#', 'items'=>array(

                              

                                

                                array('label'=>'Объявления рездела |Жильё|', 'url'=>array('category/index')),


                                'separator',

                                 ),

                                

                          

                                array('label'=>'Компании раздела |Жилье|', 'url'=>array('category/index')),

                        

                                'separator',

                             

                            array('label'=>'На сайт', 'url'=>array('site/index')),
         
   ),
                                 ),
                 ),
    ),
)); ?>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
