<?php

class UserController extends CController
{
        public $breadcrumbs;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','GeneratePdf','GenerateExcel'),
				'users'=>array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->user_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->user_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $session=new CHttpSession;
            $session->open();		
            $criteria = new CDbCriteria();            

                $model=new User('search');
                $model->unsetAttributes();  // clear any default values

                if(isset($_GET['User']))
		{
                        $model->attributes=$_GET['User'];
			
			
                   	
                       if (!empty($model->user_id)) $criteria->addCondition('user_id = "'.$model->user_id.'"');
                     
                    	
                       if (!empty($model->email)) $criteria->addCondition('email = "'.$model->email.'"');
                     
                    	
                       if (!empty($model->password)) $criteria->addCondition('password = "'.$model->password.'"');
                     
                    	
                       if (!empty($model->f_district_id)) $criteria->addCondition('f_district_id = "'.$model->f_district_id.'"');
                     
                    	
                       if (!empty($model->status)) $criteria->addCondition('status = "'.$model->status.'"');
                     
                    	
                       if (!empty($model->user_key)) $criteria->addCondition('user_key = "'.$model->user_key.'"');
                     
                    	
                       if (!empty($model->register_date)) $criteria->addCondition('register_date = "'.$model->register_date.'"');
                     
                    	
                       if (!empty($model->confirm_date)) $criteria->addCondition('confirm_date = "'.$model->confirm_date.'"');
                     
                    	
                       if (!empty($model->role)) $criteria->addCondition('role = "'.$model->role.'"');
                     
                    	
                       if (!empty($model->auth_id)) $criteria->addCondition('auth_id = "'.$model->auth_id.'"');
                     
                    	
                       if (!empty($model->service)) $criteria->addCondition('service = "'.$model->service.'"');
                     
                    	
                       if (!empty($model->fio)) $criteria->addCondition('fio = "'.$model->fio.'"');
                     
                    	
                       if (!empty($model->employment)) $criteria->addCondition('employment = "'.$model->employment.'"');
                     
                    	
                       if (!empty($model->phone)) $criteria->addCondition('phone = "'.$model->phone.'"');
                     
                    	
                       if (!empty($model->f_company_id)) $criteria->addCondition('f_company_id = "'.$model->f_company_id.'"');
                     
                    	
                       if (!empty($model->last_visit_date)) $criteria->addCondition('last_visit_date = "'.$model->last_visit_date.'"');
                     
                    	
                       if (!empty($model->login)) $criteria->addCondition('login = "'.$model->login.'"');
                     
                    	
                       if (!empty($model->sex)) $criteria->addCondition('sex = "'.$model->sex.'"');
                     
                    	
                       if (!empty($model->first_name)) $criteria->addCondition('first_name = "'.$model->first_name.'"');
                     
                    	
                       if (!empty($model->last_name)) $criteria->addCondition('last_name = "'.$model->last_name.'"');
                     
                    	
                       if (!empty($model->father_name)) $criteria->addCondition('father_name = "'.$model->father_name.'"');
                     
                    	
                       if (!empty($model->contact_email)) $criteria->addCondition('contact_email = "'.$model->contact_email.'"');
                     
                    	
                       if (!empty($model->skype)) $criteria->addCondition('skype = "'.$model->skype.'"');
                     
                    	
                       if (!empty($model->info)) $criteria->addCondition('info = "'.$model->info.'"');
                     
                    	
                       if (!empty($model->education)) $criteria->addCondition('education = "'.$model->education.'"');
                     
                    	
                       if (!empty($model->work)) $criteria->addCondition('work = "'.$model->work.'"');
                     
                    	
                       if (!empty($model->birthday)) $criteria->addCondition('birthday = "'.$model->birthday.'"');
                     
                    	
                       if (!empty($model->has_logo)) $criteria->addCondition('has_logo = "'.$model->has_logo.'"');
                     
                    	
                       if (!empty($model->member_date)) $criteria->addCondition('member_date = "'.$model->member_date.'"');
                     
                    	
                       if (!empty($model->f_repetitor_subject_id)) $criteria->addCondition('f_repetitor_subject_id = "'.$model->f_repetitor_subject_id.'"');
                     
                    	
                       if (!empty($model->update_time)) $criteria->addCondition('update_time = "'.$model->update_time.'"');
                     
                    	
                       if (!empty($model->utime)) $criteria->addCondition('utime = "'.$model->utime.'"');
                     
                    	
                       if (!empty($model->f_city_id)) $criteria->addCondition('f_city_id = "'.$model->f_city_id.'"');
                     
                    			
		}
                 $session['User_records']=User::model()->findAll($criteria); 
       

                $this->render('index',array(
			'model'=>$model,
		));

	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        public function actionGenerateExcel()
	{
            $session=new CHttpSession;
            $session->open();		
            
             if(isset($session['User_records']))
               {
                $model=$session['User_records'];
               }
               else
                 $model = User::model()->findAll();

		
		Yii::app()->request->sendFile(date('YmdHis').'.xls',
			$this->renderPartial('excelReport', array(
				'model'=>$model
			), true)
		);
	}
        public function actionGeneratePdf() 
	{
           $session=new CHttpSession;
           $session->open();
		Yii::import('application.modules.admin.extensions.giiplus.bootstrap.*');
		require_once('tcpdf/tcpdf.php');
		require_once('tcpdf/config/lang/eng.php');

             if(isset($session['User_records']))
               {
                $model=$session['User_records'];
               }
               else
                 $model = User::model()->findAll();



		$html = $this->renderPartial('expenseGridtoReport', array(
			'model'=>$model
		), true);
		
		//die($html);
		
		$pdf = new TCPDF();
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor(Yii::app()->name);
		$pdf->SetTitle('User Report');
		$pdf->SetSubject('User Report');
		//$pdf->SetKeywords('example, text, report');
		$pdf->SetHeaderData('', 0, "Report", '');
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by ".Yii::app()->name, "");
		$pdf->setHeaderFont(Array('helvetica', '', 8));
		$pdf->setFooterFont(Array('helvetica', '', 6));
		$pdf->SetMargins(15, 18, 15);
		$pdf->SetHeaderMargin(5);
		$pdf->SetFooterMargin(10);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetFont('dejavusans', '', 7);
		$pdf->AddPage();
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->LastPage();
		$pdf->Output("User_002.pdf", "I");
	}
}
