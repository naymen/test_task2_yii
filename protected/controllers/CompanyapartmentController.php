<?php

class CompanyapartmentController extends CController
{
        public $breadcrumbs;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','GeneratePdf','GenerateExcel'),
				'users'=>array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Companyapartment;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Companyapartment']))
		{
			$model->attributes=$_POST['Companyapartment'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->companyapartment_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Companyapartment']))
		{
			$model->attributes=$_POST['Companyapartment'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->companyapartment_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $session=new CHttpSession;
            $session->open();		
            $criteria = new CDbCriteria();            

                $model=new Companyapartment('search');
                $model->unsetAttributes();  // clear any default values

                if(isset($_GET['Companyapartment']))
		{
                        $model->attributes=$_GET['Companyapartment'];
			
			
                   	
                       if (!empty($model->companyapartment_id)) $criteria->addCondition('companyapartment_id = "'.$model->companyapartment_id.'"');
                     
                    	
                       if (!empty($model->discount)) $criteria->addCondition('discount = "'.$model->discount.'"');
                     
                    	
                       if (!empty($model->discount_description)) $criteria->addCondition('discount_description = "'.$model->discount_description.'"');
                     
                    	
                       if (!empty($model->weekday_from)) $criteria->addCondition('weekday_from = "'.$model->weekday_from.'"');
                     
                    	
                       if (!empty($model->weekday_to)) $criteria->addCondition('weekday_to = "'.$model->weekday_to.'"');
                     
                    	
                       if (!empty($model->holiday_from)) $criteria->addCondition('holiday_from = "'.$model->holiday_from.'"');
                     
                    	
                       if (!empty($model->holiday_to)) $criteria->addCondition('holiday_to = "'.$model->holiday_to.'"');
                     
                    	
                       if (!empty($model->event_time)) $criteria->addCondition('event_time = "'.$model->event_time.'"');
                     
                    	
                       if (!empty($model->title)) $criteria->addCondition('title = "'.$model->title.'"');
                     
                    	
                       if (!empty($model->info)) $criteria->addCondition('info = "'.$model->info.'"');
                     
                    	
                       if (!empty($model->announcement)) $criteria->addCondition('announcement = "'.$model->announcement.'"');
                     
                    	
                       if (!empty($model->has_logo)) $criteria->addCondition('has_logo = "'.$model->has_logo.'"');
                     
                    	
                       if (!empty($model->public_state)) $criteria->addCondition('public_state = "'.$model->public_state.'"');
                     
                    	
                       if (!empty($model->status)) $criteria->addCondition('status = "'.$model->status.'"');
                     
                    	
                       if (!empty($model->is_popular)) $criteria->addCondition('is_popular = "'.$model->is_popular.'"');
                     
                    	
                       if (!empty($model->decline_cause)) $criteria->addCondition('decline_cause = "'.$model->decline_cause.'"');
                     
                    	
                       if (!empty($model->advert_type)) $criteria->addCondition('advert_type = "'.$model->advert_type.'"');
                     
                    	
                       if (!empty($model->paid_before)) $criteria->addCondition('paid_before = "'.$model->paid_before.'"');
                     
                    	
                       if (!empty($model->comment_count)) $criteria->addCondition('comment_count = "'.$model->comment_count.'"');
                     
                    	
                       if (!empty($model->creation_time)) $criteria->addCondition('creation_time = "'.$model->creation_time.'"');
                     
                    	
                       if (!empty($model->update_time)) $criteria->addCondition('update_time = "'.$model->update_time.'"');
                     
                    	
                       if (!empty($model->f_category_id)) $criteria->addCondition('f_category_id = "'.$model->f_category_id.'"');
                     
                    	
                       if (!empty($model->f_user_id)) $criteria->addCondition('f_user_id = "'.$model->f_user_id.'"');
                     
                    	
                       if (!empty($model->paid_link)) $criteria->addCondition('paid_link = "'.$model->paid_link.'"');
                     
                    	
                       if (!empty($model->utime)) $criteria->addCondition('utime = "'.$model->utime.'"');
                     
                    	
                       if (!empty($model->auto_update)) $criteria->addCondition('auto_update = "'.$model->auto_update.'"');
                     
                    	
                       if (!empty($model->date_auto_update)) $criteria->addCondition('date_auto_update = "'.$model->date_auto_update.'"');
                     
                    	
                       if (!empty($model->last_auto_update)) $criteria->addCondition('last_auto_update = "'.$model->last_auto_update.'"');
                     
                    			
		}
                 $session['Companyapartment_records']=Companyapartment::model()->findAll($criteria); 
       

                $this->render('index',array(
			'model'=>$model,
		));

	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Companyapartment('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Companyapartment']))
			$model->attributes=$_GET['Companyapartment'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Companyapartment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='companyapartment-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        public function actionGenerateExcel()
	{
            $session=new CHttpSession;
            $session->open();		
            
             if(isset($session['Companyapartment_records']))
               {
                $model=$session['Companyapartment_records'];
               }
               else
                 $model = Companyapartment::model()->findAll();

		
		Yii::app()->request->sendFile(date('YmdHis').'.xls',
			$this->renderPartial('excelReport', array(
				'model'=>$model
			), true)
		);
	}
        public function actionGeneratePdf() 
	{
           $session=new CHttpSession;
           $session->open();
		Yii::import('application.modules.admin.extensions.giiplus.bootstrap.*');
		require_once('tcpdf/tcpdf.php');
		require_once('tcpdf/config/lang/eng.php');

             if(isset($session['Companyapartment_records']))
               {
                $model=$session['Companyapartment_records'];
               }
               else
                 $model = Companyapartment::model()->findAll();



		$html = $this->renderPartial('expenseGridtoReport', array(
			'model'=>$model
		), true);
		
		//die($html);
		
		$pdf = new TCPDF();
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor(Yii::app()->name);
		$pdf->SetTitle('Companyapartment Report');
		$pdf->SetSubject('Companyapartment Report');
		//$pdf->SetKeywords('example, text, report');
		$pdf->SetHeaderData('', 0, "Report", '');
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by ".Yii::app()->name, "");
		$pdf->setHeaderFont(Array('helvetica', '', 8));
		$pdf->setFooterFont(Array('helvetica', '', 6));
		$pdf->SetMargins(15, 18, 15);
		$pdf->SetHeaderMargin(5);
		$pdf->SetFooterMargin(10);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetFont('dejavusans', '', 7);
		$pdf->AddPage();
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->LastPage();
		$pdf->Output("Companyapartment_002.pdf", "I");
	}
}
